import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class Check_site_items {
    @Test
    public void check_site_items(){
        System.setProperty("webdriver.chrome.driver", "F:\\Tanya\\Testing\\QA_Auto\\check_items\\src\\main\\resources\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://service-remont.com.ua");
        String title = driver.getTitle();
        String expectedTitle = "Сервисный центр «Сервис-Ремонт» - ремонтируем бытовую технику с умом";
        Assert.assertEquals(title, expectedTitle);
        if(title.contains(expectedTitle))
            //Pass
            System.out.println("Page title contains: " + expectedTitle);
        else
            //Fail
            System.out.println("Page title doesn't contains: " + expectedTitle);
        assertTrue(driver.findElement(By.xpath("//h2[contains(text(),'Мелкогабаритная бытовая техника')]")).isDisplayed());
        assertTrue(driver.findElement(By.xpath("//h2[contains(text(),'Портативная техника')]")).isDisplayed());
        assertTrue(driver.findElement(By.xpath("//h2[contains(text(),'Apple')]")).isDisplayed());
        assertTrue(driver.findElement(By.xpath("//h2[contains(text(),'Мониторы и телевизоры')]")).isDisplayed());
        driver.quit();
    }
}
